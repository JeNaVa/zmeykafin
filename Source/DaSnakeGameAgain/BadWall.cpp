// Fill out your copyright notice in the Description page of Project Settings.


#include "BadWall.h"

// Sets default values
ABadWall::ABadWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABadWall::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(RandomRotation());
}

// Called every frame
void ABadWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABadWall::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		Interactor->Destroy();
	}
}

FRotator ABadWall::RandomRotation()
{
	int32 RandomXRotation = FMath::RandRange(0,90);
	FRotator Rotation(0, RandomXRotation, 0);
	return Rotation;
}


