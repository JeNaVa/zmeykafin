// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DaSnakeGameAgainGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DASNAKEGAMEAGAIN_API ADaSnakeGameAgainGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
