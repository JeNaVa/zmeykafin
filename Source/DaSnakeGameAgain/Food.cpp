// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "FoodSpeedBoost.h"
#include "SuperFood.h"
#include "WallDeboost.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(1);
			SpawnFood();
			PlaySound();
			Destroy(true);
		}
	}
}


FTransform AFood::GetRandomTransform()
{
	int32 RandomY = FMath::RandRange(-1684,2410);
	int32 RandomX = FMath::RandRange(-2102,1987);
	const int32 Height = 13;
	FVector SpawnZone(RandomX, RandomY, Height);
	FTransform SpawnTransform(SpawnZone);
	return SpawnTransform;
}

void AFood::PlaySound_Implementation()
{
}


void AFood::SpawnFood()
{

	int32 PerecentToSpawnSpeedBooster = FMath::RandRange(0,100);
	int32 PerecentToSpawnSuperFood = FMath::RandRange(0,100);
	int32 PerecentToSpawnBadWall = FMath::RandRange(0,100);
	if (PerecentToSpawnSpeedBooster >= 60)
	{
		GetWorld()->SpawnActor<AFoodSpeedBoost>(FoodSpeedBoostClass, GetRandomTransform());
	}
	else if (PerecentToSpawnSuperFood >= 80)
	{
		GetWorld()->SpawnActor<ASuperFood>(SuperFoodClass, GetRandomTransform());
	}
	else if (PerecentToSpawnBadWall >= 85)
	{
		GetWorld()->SpawnActor<AWallDeboost>(WallDeBoostClass, GetRandomTransform());
	}
	GetWorld()->SpawnActor<AFood>(FoodClass, GetRandomTransform());
}



