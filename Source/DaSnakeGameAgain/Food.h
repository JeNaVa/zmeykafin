// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Food.generated.h"

class AWallDeboost;
class ASuperFood;
class AFoodSpeedBoost;

UCLASS()
class DASNAKEGAMEAGAIN_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWallDeboost> WallDeBoostClass;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodSpeedBoost> FoodSpeedBoostClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASuperFood> SuperFoodClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
	void SpawnFood();

	UFUNCTION()
	FTransform GetRandomTransform();

	UFUNCTION(BlueprintNativeEvent)
	void PlaySound();
};
