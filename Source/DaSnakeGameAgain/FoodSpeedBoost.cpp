// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpeedBoost.h"
#include "SnakeBase.h"
#include "Food.h"
#include "GenericPlatform/GenericPlatformCrashContext.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AFoodSpeedBoost::AFoodSpeedBoost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSpeedBoost::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSpeedBoost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AFoodSpeedBoost::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto* Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(1);
			if (Snake->MovementSpeed >= 0.05f)
			{
				Snake->MovementSpeed -= 0.03f;
			}
			PlaySound();
			Destroy(true);
		}
	}
}

void AFoodSpeedBoost::PlaySound_Implementation()
{
}


