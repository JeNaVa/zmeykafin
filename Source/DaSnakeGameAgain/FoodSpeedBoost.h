// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "Food.h"
#include "GameFramework/Actor.h"
#include "FoodSpeedBoost.generated.h"

class IInteractable;
class ASnakeBase;

UCLASS()
class DASNAKEGAMEAGAIN_API AFoodSpeedBoost : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodSpeedBoost();

	UPROPERTY()
	TSubclassOf<ASnakeBase> SnakeActorClass;
	
	

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION(BlueprintNativeEvent)
	void PlaySound();
	
};
