// Fill out your copyright notice in the Description page of Project Settings.


#include "SuperFood.h"
#include "SnakeBase.h"

ASuperFood::ASuperFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ASuperFood::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASuperFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASuperFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(3);
			PlaySound();
			Destroy(true);
		}
	}
}

void ASuperFood::PlaySound_Implementation()
{
}


