// Fill out your copyright notice in the Description page of Project Settings.


#include "WallDeboost.h"
#include "BadWall.h"


// Sets default values
AWallDeboost::AWallDeboost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWallDeboost::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallDeboost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallDeboost::Interact(AActor* Interactor, bool bIsHead)
{
	GetWorld()->SpawnActor<ABadWall>(BadWall, GetRandomTransform());
	PlaySound();
	Destroy(true);
}

FTransform AWallDeboost::GetRandomTransform()
{
	int32 RandomY = FMath::RandRange(-1684,2410);
	int32 RandomX = FMath::RandRange(-2102,1987);
	const int32 Height = 13;
	FVector SpawnZone(RandomX, RandomY, Height);
	FTransform SpawnTransform(SpawnZone);
	return SpawnTransform;
}

void AWallDeboost::PlaySound_Implementation()
{
}




